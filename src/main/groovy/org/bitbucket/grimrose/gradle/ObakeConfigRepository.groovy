package org.bitbucket.grimrose.gradle

import groovy.json.JsonSlurper


class ObakeConfigRepository {

    Map holder = [:]

    ObakeConfigRepository(String fileName) {
        def text = ObakeConfigRepository.class.classLoader.getResourceAsStream(fileName).text
        holder = new JsonSlurper().parseText(text)
    }

    ObakeConfigRepository() {
        this('config.json')
    }

    static ObakeConfigRepository create(String fileName) {
        new ObakeConfigRepository(fileName)
    }

    Object findBy(String key) {
        holder[key] ?: ''
    }

}
