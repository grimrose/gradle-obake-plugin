package org.bitbucket.grimrose.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project


class ObakePlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        project.extensions.create 'obake', ObakePluginExtension
        project.task 'preparePhantomJS', type: PreparePhantomJSTask
        project.task 'phantomJSAgent', type: PhantomJSAgentTask
    }

}
