package org.bitbucket.grimrose.gradle

import org.gradle.api.InvalidUserDataException
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging


class PhantomJSAgent {

    Logger logger = Logging.getLogger(PhantomJSAgent)

    String binaryPath

    List<String> options = []

    static PhantomJSAgent make(String binaryPath) {
        new PhantomJSAgent(binaryPath: binaryPath)
    }

    PhantomJSAgent adopt(String... options) {
        this.options = options.toList()
        this
    }

    String execute(scriptPath, String... args) {
        assert binaryPath

        final specificOptions = ['--help', '-h', '--version', '-v']
        boolean unnecessaryScript = options.contains(specificOptions)

        List commandLine = [binaryPath]
        if (unnecessaryScript) {
            commandLine += options.findAll { specificOptions.contains(it) }
        } else {
            commandLine += (options + scriptPath << args).flatten()
        }

        logger.lifecycle "command is $commandLine"

        def process = commandLine.execute()
        process.waitFor()
        if (process.exitValue()) {
            throw new InvalidUserDataException(process.err.text)
        }
        process.in.text
    }

}
