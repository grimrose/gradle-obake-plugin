package org.bitbucket.grimrose.gradle

import org.apache.commons.lang.SystemUtils
import org.gradle.api.tasks.TaskExecutionException
import org.gradle.api.tasks.TaskValidationException
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Shared
import spock.lang.Specification

class PhantomJSAgentTaskSpec extends Specification {

    @Shared
    PhantomJSVersion version = PhantomJSVersion._1_9_7

    @Shared
    File dir

    def setupSpec() {
        def project = ProjectBuilder.builder().build()
        project.apply plugin: 'obake'


        ObakeConfig config = new ObakeConfig()
        config.apply(project.extensions.findByType(ObakePluginExtension))

        File archiveResources = new File(SystemUtils.USER_DIR, 'src/test/resources/')
        def archive = new File(archiveResources, config.makeArchiveFileName(version))

        def conventionPath = new ConventionPathResolver().find(project.projectDir, config)
        dir = project.file(conventionPath)
        new ArchiveExtractor(config: config, ant: project.ant).unpack(dir, archive.path)
    }

    PhantomJSAgentTask sut

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder()

    String helloJs
    String countdownJs
    String outputEncodingJs
    String argumentsJs

    def setup() {
        def project = ProjectBuilder.builder().build()
        project.apply plugin: 'obake'

        ObakeConfig config = new ObakeConfig()
        def extension = project.extensions.findByType(ObakePluginExtension)
        extension.phantomjsBinaryPath = config.makeBinaryPath(dir.path)
        project.file(extension.phantomjsBinaryPath).setExecutable(true)
        config.apply(extension)

        helloJs = new File(dir, 'examples/hello.js').absolutePath
        countdownJs = new File(dir, 'examples/countdown.js').absolutePath
        outputEncodingJs = new File(dir, 'examples/outputEncoding.js').absolutePath
        argumentsJs = new File(dir, 'examples/arguments.js').absolutePath

        sut = project.tasks.findByName('phantomJSAgent') as PhantomJSAgentTask
    }

    def "task should fail when target js is null"() {
        when:
        sut.execute()

        then:
        thrown(TaskValidationException)
    }

    def "task should fail when target js not found"() {
        given:
        sut.js = 'hoge'

        when:
        sut.execute()

        then:
        thrown(TaskExecutionException)
    }

    def "task should execute"() {
        given:
        sut.js = helloJs

        when:
        sut.execute()

        then:
        noExceptionThrown()
    }

    def "task should output file"() {
        given:
        def file = temporaryFolder.newFile('result.txt')

        sut.js = countdownJs
        sut.outputFile = file

        when:
        sut.execute()

        println file.text
        then:
        file.length()
    }

    def "task should execute with option"() {
        given:
        sut.js = outputEncodingJs
        sut.options = ['--output-encoding=sjis']

        when:
        sut.execute()

        then:
        noExceptionThrown()
    }

    def "task should execute with arguments"() {
        given:
        def file = temporaryFolder.newFile('result.txt')

        sut.js = argumentsJs
        sut.outputFile = file
        sut.args = ['a', 'b', 'c']

        when:
        sut.execute()

        then:
        file.text == """0: $argumentsJs
1: a
2: b
3: c
"""
    }

}
