package org.bitbucket.grimrose.gradle

import org.apache.commons.lang.SystemUtils
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Shared
import spock.lang.Specification

class PreparePhantomJSTaskSpec extends Specification {

    @Shared
    PhantomJSVersion version = PhantomJSVersion._1_9_7

    File archiveResources = new File(SystemUtils.USER_DIR, 'src/test/resources/')
    Project project
    PreparePhantomJSTask sut

    def setup() {
        project = ProjectBuilder.builder().build()
        project.apply plugin: 'obake'
        sut = project.tasks.findByName('preparePhantomJS') as PreparePhantomJSTask
        ObakeConfig spy = Spy(ObakeConfig)
        spy.makeDownloadUrl(_) >> new File(archiveResources, sut.config.makeArchiveFileName(version)).toURI().toURL()
        spy.apply(project.extensions.findByType(ObakePluginExtension))
        sut.config = spy
    }

    def "tools path should be not found"() {
        when:
        def actual = sut.validateBinaryPath()

        then:
        actual == ''
    }

    def "archive should be downloaded"() {
        when:
        sut.download()

        then:
        noExceptionThrown()
    }

    def "archive should be unpacked"() {
        given:
        String path = new File(archiveResources, sut.config.makeArchiveFileName(version)).absolutePath

        when:
        sut.unpack(path)

        then:
        def actual = project.file(sut.makeConventionPath()).list()
        println "$actual"
        actual.size() != 0
    }

    def "should be prepared"() {
        when:
        sut.process()

        then:
        def path = project.file(sut.makeConventionPath()).absolutePath
        [new File(path, '/bin/phantomjs').absolutePath, path + '/examples/hello.js'].execute().in.text == 'Hello, world!\n'
    }

}
